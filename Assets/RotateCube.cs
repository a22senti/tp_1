using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.InputSystem;

public class RotateCube : MonoBehaviour
{
    public float speed = 3.0f;
    public  InputAction activateRotation;
    public Boolean b = false;
    // Start is called before the first frame update
    void Start()
    {
        activateRotation.Enable();
            activateRotation.started += ctx => b= !b ;


    }

    // Update is called once per frame
    void Update()
    {
        if (b){
            transform.Rotate(Vector3.up,3); /* Action was started */
    }
       
        Debug.Log ("Il y a un changement ");
    }
}
