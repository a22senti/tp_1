using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseInteraction : MonoBehaviour ,IPointerClickHandler, IPointerEnterHandler,IPointerExitHandler
{
    public Rigidbody rigid;
    public Renderer render;
    public Color initial_color;
    public void OnPointerClick(PointerEventData eventData)
    {
        Vector3 v =Camera.main.transform.forward ;
        rigid.AddForce(v*50000);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        render.material.color=Color.red;
        }

    public void OnPointerExit(PointerEventData eventData)
    {
        render.material.color=initial_color;

    }

    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody>() ;
        render = GetComponent<Renderer>();
        initial_color = render.material.color;

    }

    // Update is called once per frame
    void Update()
    {

        
    }
}
