using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

public class CreateOnClick : MonoBehaviour
{
     public GameObject aCopier;
     public  InputAction CreateCube;

    // Start is called before the first frame update
    void Start()
    {
    CreateCube.Enable();
    CreateCube.started += ctx => creer() ;


    }
    void creer(){
        GameObject new_obj =Instantiate(aCopier);
        new_obj.GetComponent<Renderer>().material.color = Color.yellow; 
        new_obj.transform.position= Camera.main.transform.position+Camera.main.transform.forward*50;
           }
    // Update is called once per frame
    void Update()
    {
        
    }
}
